<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Doctrine\ORM\Tools\SchemaTool;
use App\Entity\Book;
/**
 * Les tests fonctionnels permettent de tester les 
 * fonctionnalités de l'application comme si on l'utilisait pour de 
 * vrai. Grâce à symfony, on va pouvoir utiliser un objet pour
 * naviguer sur les différentes pages, soumettre des formulaires,
 * vérifier ce qui est affiché sur chaque page etc.
 * Ces tests sont très intéressants car ils permettent de voir en
 * permanence si les modifications que l'on fait sur le projet cassent
 * des choses existantes ou non.
 */
class AddBookControllerTest extends WebTestCase
{
    /**
     * Dans le setUp, on va faire en sorte de remettre à zéro 
     * la base de données utilisée pour les tests et d'y créer
     * les tables. (pour utiliser une base de données propre au
     * tests, il faut rajouter un fichier doctrine.yaml dans le
     * dossier config/packages/test. Voir le fichier en question
     * pour la configuration)
     */
    public function setUp() {
        $client = static::createClient();
        //Une manière de choper l'entity manager depuis un test
        $manager = $client->getContainer()
                            ->get("doctrine")->getManager();
        //On crée une instance de SchemaTool qui nous permettra de créer
        //la base de donnée à partir de nos entités
        $schema = new SchemaTool($manager);
        //On vide la base de données
        $schema->dropDatabase();
        //On recréer les tables à partir des métadatas de nos entités
        $schema->updateSchema(
            $manager->getMetadataFactory()->getAllMetadata()
        );
        

    }

    public function testPageWorking()
    {
        //On crée le client qui nous permettra de naviguer sur les pages
        //d'obtenir les reponses http et autre.
        $client = static::createClient();
        //On dit au client de naviguer sur la route /add/book en GET.
        //celui ci nous renvoie un crawler qui permettra de parcourir
        //le DOM de la page si la réponse est de type HTML
        $crawler = $client->request('GET', '/add/book');
        //On fait une assertion pour vérifier si la réponse de la requête est bien 200 (success)
        $this->assertSame(200, $client->getResponse()->getStatusCode());
        //On peut utiliser le crawler pour capturer des éléments html et faire
        //des assertions sur leur nombre, leur contenu etc.
        $this->assertSame(1, $crawler->filter("form")->count());
        $this->assertSame(4, $crawler->filter("input")->count());
    }

    public function testBookAdding() {
        $client = static::createClient();

        $crawler = $client->request('GET', '/add/book');
        //On récupère le formulaire à partir de son bouton grâce au crawler
        $form = $crawler->selectButton("Add")->form();
        //On assigne les valeurs du formulaire (book devra être remplacé
        //par le nom de l'entité dont on fait le formulaire si c'est pas un book)
        $form["book[title]"] = "Teste Titre";
        $form["book[content]"] = "Test Contenu";
        $form["book[author]"] = "Test Auteur";
        $form["book[pageNumber]"] = 543;
        //On soumet le formulaire
        $client->submit($form);
        //On vérifie si la soumission nous a bien renvoyé un code 200 (success)
        $this->assertSame(200, $client->getResponse()->getStatusCode());
        //On récupère le repository des Book
        $repo = $client->getContainer()->get('doctrine')->getRepository(Book::class);
        //On fait une assertion pour voir si on a bien un élément dans la table book
        $this->assertCount(1, $repo->findAll());
    }
}
