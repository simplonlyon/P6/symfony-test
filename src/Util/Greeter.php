<?php

namespace App\Util;

class Greeter {

    public function greet(string $name):string {
        
        return "Hello $name, how's it going ?";
    }
}